import { AdditionalStat, ItemAction } from '../../../interfaces/additional';
import { SWADE } from '../../config';
import SwadeDice from '../../dice';
import SwadeActor from '../../entities/SwadeActor';
import SwadeItem from '../../entities/SwadeItem';
import ItemChatCardHelper from '../../ItemChatCardHelper';

export default class CharacterSheet extends ActorSheet {
  static get defaultOptions() {
    return {
      ...super.defaultOptions,
      classes: ['swade-official', 'sheet', 'actor'],
      width: 630,
      height: 700,
      resizable: true,
      tabs: [
        {
          navSelector: '.tabs',
          contentSelector: '.sheet-body',
          initial: 'summary',
        },
      ],
    };
  }

  get template() {
    return 'systems/swade/templates/official/sheet.html';
  }

  /**
   * @override
   */
  get actor(): SwadeActor {
    return super.actor as SwadeActor;
  }

  activateListeners(html: JQuery): void {
    super.activateListeners(html);

    // Everything below here is only needed if the sheet is editable
    if (!this.options.editable) return;

    // Drag events for macros.
    //@ts-ignore
    if (this.actor.isOwner) {
      const handler = (ev) => this._onDragStart(ev);
      // Find all items on the character sheet.
      html.find('li.item.skill').each((i, li) => {
        // Add draggable attribute and dragstart listener.
        li.setAttribute('draggable', 'true');
        li.addEventListener('dragstart', handler, false);
      });
      html.find('li.item.weapon').each((i, li) => {
        // Add draggable attribute and dragstart listener.
        li.setAttribute('draggable', 'true');
        li.addEventListener('dragstart', handler, false);
      });
      html.find('li.item.armor').each((i, li) => {
        // Add draggable attribute and dragstart listener.
        li.setAttribute('draggable', 'true');
        li.addEventListener('dragstart', handler, false);
      });
      html.find('li.item.shield').each((i, li) => {
        // Add draggable attribute and dragstart listener.
        li.setAttribute('draggable', 'true');
        li.addEventListener('dragstart', handler, false);
      });
      html.find('li.item.misc').each((i, li) => {
        // Add draggable attribute and dragstart listener.
        li.setAttribute('draggable', 'true');
        li.addEventListener('dragstart', handler, false);
      });
      html.find('li.item.power').each((i, li) => {
        // Add draggable attribute and dragstart listener.
        li.setAttribute('draggable', 'true');
        li.addEventListener('dragstart', handler, false);
      });
      html.find('li.active-effect').each((i, li) => {
        // Add draggable attribute and dragstart listener.
        li.setAttribute('draggable', 'true');
        li.addEventListener('dragstart', handler, false);
      });
      html.find('li.item.edge-hindrance').each((i, li) => {
        // Add draggable attribute and dragstart listener.
        li.setAttribute('draggable', 'true');
        li.addEventListener('dragstart', handler, false);
      });
    }

    //Toggle Conviction
    html.find('.conviction-toggle').on('click', async () => {
      const current = getProperty(
        this.actor.data,
        'data.details.conviction.value',
      ) as number;
      const active = getProperty(
        this.actor.data,
        'data.details.conviction.active',
      ) as boolean;

      if (current > 0 && !active) {
        await this.actor.update({
          'data.details.conviction.value': current - 1,
          'data.details.conviction.active': true,
        });
        ChatMessage.create({
          speaker: {
            actor: this.actor.id,
            alias: this.actor.name,
          },
          content: game.i18n.localize('SWADE.ConvictionActivate'),
        });
      } else {
        await this.actor.update({
          'data.details.conviction.active': false,
        });
      }
    });

    html.find('.add-benny').on('click', () => {
      this.actor.getBenny();
    });

    html.find('.spend-benny').on('click', () => {
      this.actor.spendBenny();
    });

    //Roll Attribute
    html.find('.attribute-label').on('click', (ev) => {
      const element = ev.currentTarget as Element;
      const attribute = element.parentElement.dataset.attribute;
      this.actor.rollAttribute(attribute);
    });

    //Toggle Equipment Card collapsible
    html.find('.skill-card .skill-name.item-name').on('click', (ev) => {
      $(ev.currentTarget)
        .parents('.item.skill.skill-card')
        .find('.card-content')
        .slideToggle();
    });

    // Roll Skill
    html.find('.skill-card .skill-die').on('click', (ev) => {
      const element = ev.currentTarget as HTMLElement;
      const item = element.parentElement.dataset.itemId;
      this.actor.rollSkill(item);
    });

    //Running Die
    html.find('.running-die').on('click', () => {
      const runningDie = getProperty(
        this.actor.data,
        'data.stats.speed.runningDie',
      );
      const runningMod = getProperty(
        this.actor.data,
        'data.stats.speed.runningMod',
      );
      const pace = getProperty(this.actor.data, 'data.stats.speed.value');
      let rollFormula = `1d${runningDie}`;

      rollFormula = rollFormula.concat(`+${pace}`);

      if (runningMod && runningMod !== 0) {
        rollFormula = rollFormula.concat(runningMod);
      }

      new Roll(rollFormula).evaluate({ async: false }).toMessage({
        speaker: ChatMessage.getSpeaker({ actor: this.actor }),
        flavor: game.i18n.localize('SWADE.Running'),
      });
    });

    // Roll Damage
    html.find('.damage-roll').on('click', (ev) => {
      const li = $(ev.currentTarget).parents('.item');
      const item = this.actor.items.get(li.data('itemId'));
      return item.rollDamage();
    });

    //Toggle Equipment Card collapsible
    html.find('.gear-card .item-name').on('click', (ev) => {
      $(ev.currentTarget)
        .parents('.gear-card')
        .find('.card-content')
        .slideToggle();
    });

    //Edit Item
    html.find('.item-edit').on('click', (ev) => {
      const li = $(ev.currentTarget).parents('.item');
      const item = this.actor.items.get(li.data('itemId'));
      item.sheet.render(true);
    });

    //Show Item
    html.find('.item-show').on('click', (ev) => {
      const li = $(ev.currentTarget).parents('.item');
      const item = this.actor.items.get(li.data('itemId'));
      item.show();
    });

    // Delete Item
    html.find('.item-delete').on('click', async (ev) => {
      const li = $(ev.currentTarget).parents('.item');
      const ownedItem = this.actor.items.get(li.data('itemId'));
      const template = `
      <form>
        <div style="text-align: center;">
          <p>
          ${game.i18n.localize('Delete')} <strong>${ownedItem.name}</strong>?
          </p>
        </div>
      </form>`;
      await Dialog.confirm({
        title: game.i18n.localize('Delete'),
        content: template,
        yes: () => {
          li.slideUp(200, () => this.actor.deleteOwnedItem(ownedItem.id));
        },
        no: () => {},
      });
    });

    html.find('.item-create').on('click', async (ev) => {
      const header = ev.currentTarget;
      const type = header.dataset.type;

      // item creation helper func
      const createItem = function (
        type: string,
        name: string = `New ${type.capitalize()}`,
      ): any {
        const itemData = {
          name: name ? name : `New ${type.capitalize()}`,
          type: type,
          data: header.dataset,
        };
        delete itemData.data['type'];
        return itemData;
      };
      switch (type) {
        case 'choice':
          this._chooseItemType().then(async (dialogInput: any) => {
            if (dialogInput.type !== 'effect') {
              const itemData = createItem(dialogInput.type, dialogInput.name);
              await this.actor.createOwnedItem(itemData, { renderSheet: true });
            } else {
              this._createActiveEffect(dialogInput.name);
            }
          });
          break;
        case 'effect':
          this._createActiveEffect();
          break;
        default:
          await this.actor.createOwnedItem(createItem(type), {
            renderSheet: true,
          });
          break;
      }
    });

    //Toggle Equipment Status
    html.find('.item-toggle').on('click', async (ev) => {
      const li = $(ev.currentTarget).parents('.item');
      const itemID = li.data('itemId');
      const item = this.actor.items.get(itemID);
      await this.actor.updateOwnedItem(this._toggleEquipped(itemID, item));
    });

    html.find('.effect-action').on('click', async (ev) => {
      const a = ev.currentTarget;
      const effectId = a.closest('li').dataset.effectId;
      const effect = this.actor.effects.get(effectId);
      const action = a.dataset.action;
      let item: SwadeItem = null;

      switch (action) {
        case 'edit':
          return effect.sheet.render(true);
        case 'delete':
          return effect.delete();
        case 'toggle':
          return effect.update({ disabled: !effect.data.disabled });
        case 'open-origin':
          item = (await fromUuid(effect.data.origin)) as SwadeItem;
          if (item) this.actor.items.get(item.id).sheet.render(true);
          break;
        default:
          console.warn(`The action ${action} is not currently supported`);
          break;
      }
    });

    html.find('.edge-hindrance .item-name button').on('click', (ev) => {
      $(ev.currentTarget)
        .parents('.edge-hindrance')
        .find('.description')
        .slideToggle();
    });

    html.find('.power .item-name button').on('click', (ev) => {
      $(ev.currentTarget).parents('.power').find('.description').slideToggle();
    });

    html.find('.armor-display').on('click', () => {
      const armorPropertyPath = 'data.stats.toughness.armor';
      const armorvalue = getProperty(this.actor.data, armorPropertyPath);
      const label = game.i18n.localize('SWADE.Armor');
      const template = `
      <form><div class="form-group">
        <label>${game.i18n.localize('SWADE.Ed')} ${label}</label>
        <input name="modifier" value="${armorvalue}" type="number"/>
      </div></form>`;

      new Dialog({
        title: `${game.i18n.localize('SWADE.Ed')} ${this.actor.name} ${label}`,
        content: template,
        buttons: {
          ok: {
            icon: '<i class="fas fa-check"></i>',
            label: game.i18n.localize('SWADE.Ok'),
            callback: (html: JQuery) => {
              const newData = {};
              newData[armorPropertyPath] = html
                .find('input[name="modifier"]')
                .val();
              this.actor.update(newData);
            },
          },
          cancel: {
            icon: '<i class="fas fa-times"></i>',
            label: game.i18n.localize('Cancel'),
          },
        },
        default: 'ok',
      }).render(true);
    });

    html.find('.parry-display').on('click', () => {
      const parryPropertyPath = 'data.stats.parry.modifier';
      const parryMod = getProperty(
        this.actor.data,
        parryPropertyPath,
      ) as number;
      const label = game.i18n.localize('SWADE.Parry');
      const template = `
      <form><div class="form-group">
        <label>${game.i18n.localize('SWADE.Ed')} ${label}</label>
        <input name="modifier" value="${parryMod}" type="number"/>
      </div></form>`;

      new Dialog({
        title: `${game.i18n.localize('SWADE.Ed')} ${this.actor.name} ${label}`,
        content: template,
        buttons: {
          ok: {
            icon: '<i class="fas fa-check"></i>',
            label: game.i18n.localize('SWADE.Ok'),
            callback: (html: JQuery) => {
              const newData = {};
              newData[parryPropertyPath] = html
                .find('input[name="modifier"]')
                .val() as number;
              this.actor.update(newData);
            },
          },
          cancel: {
            icon: '<i class="fas fa-times"></i>',
            label: game.i18n.localize('Cancel'),
          },
        },
        default: 'ok',
      }).render(true);
    });

    //Item Action Buttons
    html.find('.card-buttons button').on('click', async (ev) => {
      const button = ev.currentTarget;
      const action = button.dataset['action'];
      const itemId = $(button).parents('.chat-card.item-card').data().itemId;
      const item = this.actor.items.get(itemId);
      const additionalMods = [];
      const ppToAdjust = $(button)
        .parents('.chat-card.item-card')
        .find('input.pp-adjust')
        .val() as string;

      //if it's a power and the No Power Points rule is in effect
      if (
        item.type === 'power' &&
        game.settings.get('swade', 'noPowerPoints')
      ) {
        let modifier = Math.ceil(parseInt(ppToAdjust, 10) / 2);
        modifier = Math.min(modifier * -1, modifier);
        const actionObj = getProperty(
          item.data,
          `data.actions.additional.${action}.skillOverride`,
        ) as ItemAction;
        //filter down further to make sure we only apply the penalty to a trait roll
        if (
          action === 'formula' ||
          (!!actionObj && actionObj.type === 'skill')
        ) {
          additionalMods.push(modifier.signedString());
        }
      }

      ItemChatCardHelper.handleAction(item, this.actor, action, additionalMods);

      //handle Power Item Card PP adjustment
      if (action === 'pp-adjust') {
        const adjustment = button.getAttribute('data-adjust') as string;
        const power = this.actor.items.get(itemId);
        let key = 'data.powerPoints.value';
        const arcane = getProperty(power.data, 'data.arcane');
        if (arcane) key = `data.powerPoints.${arcane}.value`;
        let newPP = getProperty(this.actor.data, key);
        if (adjustment === 'plus') {
          newPP += parseInt(ppToAdjust, 10);
        } else if (adjustment === 'minus') {
          newPP -= parseInt(ppToAdjust, 10);
        }
        await this.actor.update({ [key]: newPP });
      }
    });

    //Additional Stats roll
    html.find('.additional-stats .roll').on('click', (ev) => {
      const button = ev.currentTarget;
      const stat = button.dataset.stat;
      const statData = getProperty(
        this.actor.data,
        `data.additionalStats.${stat}`,
      ) as AdditionalStat;
      let modifier = statData.modifier || '';
      if (!!modifier && !modifier.match(/^[+-]/)) {
        modifier = '+' + modifier;
      }
      //return early if there's no data to roll
      if (!statData.value) return;
      new Roll(`${statData.value}${modifier}`, this.actor.getRollData())
        .evaluate({ async: false })
        .toMessage({
          speaker: ChatMessage.getSpeaker(),
          flavor: statData.label,
        });
    });

    //Wealth Die Roll
    html.find('.currency .roll').on('click', () => {
      const die: number =
        getProperty(this.actor.data, 'data.details.wealth.die') || 6;
      const mod: number =
        getProperty(this.actor.data, 'data.details.wealth.modifier') || 0;
      const wildDie: number =
        getProperty(this.actor.data, 'data.details.wealth.wild-die') || 6;
      const modString = mod !== 0 ? mod.signedString() : '';
      const dieLabel = game.i18n.localize('SWADE.WealthDie');
      const wildDieLabel = game.i18n.localize('SWADE.WildDie');
      const formula = `{1d${die}x[${dieLabel}], 1d${wildDie}x[${wildDieLabel}]}kh${modString}`;
      const roll = new Roll(formula);

      SwadeDice.Roll({
        roll: roll,
        speaker: ChatMessage.getSpeaker(),
        actor: this.actor,
        flavor: game.i18n.localize('SWADE.WealthDie'),
        title: game.i18n.localize('SWADE.WealthDie'),
      });
    });
  }

  getData() {
    const data: any = super.getData();

    data.bennyImageURL = SWADE.bennies.sheetImage;
    data.itemsByType = {};
    for (const type of game.system.entityTypes.Item) {
      data.itemsByType[type] = data.items.filter((i) => i.type === type) || [];
    }

    for (const type of Object.keys(data.itemsByType)) {
      for (const item of data.itemsByType[type]) {
        // Basic template rendering data
        const ammoManagement = game.settings.get('swade', 'ammoManagement');
        item.shots = getProperty(item, 'data.shots');
        item.currentShots = getProperty(item, 'data.currentShots');

        item.isMeleeWeapon =
          'weapon' &&
          ((!item.shots && !item.currentShots) ||
            (item.shots === '0' && item.currentShots === '0'));

        const actions = getProperty(item, 'data.actions.additional');
        item.hasAdditionalActions =
          !!actions && Object.keys(actions).length > 0;

        item.actions = [];

        for (const action in actions) {
          item.actions.push({
            key: action,
            type: actions[action].type,
            name: actions[action].name,
          });
        }

        item.actor = data.actor;
        item.config = SWADE;
        item.hasAmmoManagement =
          item.type === 'weapon' &&
          !item.isMeleeWeapon &&
          ammoManagement &&
          !getProperty(item, 'data.autoReload');
        item.hasReloadButton =
          ammoManagement &&
          item.type === 'weapon' &&
          getProperty(item, 'data.shots') > 0 &&
          !getProperty(item, 'data.autoReload');
        item.hasDamage =
          !!getProperty(item, 'data.damage') ||
          !!item.actions.find((action) => action.type === 'damage');
        item.skill =
          getProperty(item, 'data.actions.skill') ||
          !!item.actions.find((action) => action.type === 'skill');
        item.hasSkillRoll =
          ['weapon', 'power', 'shield'].includes(item.type) &&
          !!getProperty(item, 'data.actions.skill');
        item.powerPoints = this.getPowerPoints(item);
      }
    }

    //sort skills alphabetically
    data.sortedSkills = data.itemsByType['skill'];
    data.sortedSkills.sort((a, b) => a.name.localeCompare(b.name));

    data.currentBennies = [];
    const bennies = getProperty(
      this.actor.data,
      'data.bennies.value',
    ) as number;
    for (let i = 0; i < bennies; i++) {
      data.currentBennies.push(i + 1);
    }

    const additionalStats = data.data.data.additionalStats || {};
    for (const attr of Object.values(additionalStats)) {
      attr['isCheckbox'] = attr['dtype'] === 'Boolean';
    }
    data.hasAdditionalStatsFields = Object.keys(additionalStats).length > 0;

    //Encumbrance
    data.inventoryWeight = this._calcInventoryWeight([
      ...data.itemsByType['gear'],
      ...data.itemsByType['weapon'],
      ...data.itemsByType['armor'],
      ...data.itemsByType['shield'],
    ]);
    data.maxCarryCapacity = this.actor.calcMaxCarryCapacity();

    //Deal with ABs and Powers
    const powers = {
      arcanes: {},
      arcanesCount: data.itemsByType.power
        .map((p) => {
          return p.data.arcane;
        })
        .filter(Boolean).length,
      hasPowersWithoutArcane:
        data.itemsByType.power.reduce((acc, cur) => {
          if (cur.data.arcane) {
            return acc;
          } else {
            return (acc += 1);
          }
        }, 0) > 0,
    };

    for (const power of data.itemsByType.power) {
      const arcane = power.data.arcane;
      if (!arcane) {
        continue;
      }
      if (!powers.arcanes[arcane]) {
        powers.arcanes[arcane] = {
          valuePath: `data.powerPoints.${arcane}.value`,
          value: getProperty(
            this.actor.data,
            `data.powerPoints.${arcane}.value`,
          ),
          maxPath: `data.powerPoints.${arcane}.max`,
          max: getProperty(this.actor.data, `data.powerPoints.${arcane}.max`),
          powers: [],
        };
      }
      powers.arcanes[arcane].powers.push(power);
    }

    data.powers = powers;

    const shields = data.itemsByType['shield'];
    data.parry = 0;
    if (shields) {
      shields.forEach((shield: SwadeItem) => {
        if (shield.data['equipped']) {
          data.parry += parseInt(shield.data['parry']);
        }
      });
    }
    // Check for enabled optional rules
    data.settingrules = {
      conviction: game.settings.get('swade', 'enableConviction'),
      noPowerPoints: game.settings.get('swade', 'noPowerPoints'),
      wealthType: game.settings.get('swade', 'wealthType'),
      currencyName: game.settings.get('swade', 'currencyName'),
    };
    return data;
  }

  getPowerPoints(item) {
    if (item.type !== 'power') return {};

    const arcane = getProperty(item, 'data.arcane');
    let current = getProperty(item.actor, 'data.powerPoints.value');
    let max = getProperty(item.actor, 'data.powerPoints.max');
    if (arcane) {
      current = getProperty(item.actor, `data.powerPoints.${arcane}.value`);
      max = getProperty(item.actor, `data.powerPoints.${arcane}.max`);
    }
    return { current, max };
  }

  /**
   * Extend and override the sheet header buttons
   * @override
   */
  protected _getHeaderButtons() {
    let buttons = super._getHeaderButtons();

    // Token Configuration
    const canConfigure = game.user.isGM || this.actor.owner;
    if (this.options.editable && canConfigure) {
      buttons = [
        {
          label: game.i18n.localize('SWADE.Tweaks'),
          class: 'configure-actor',
          icon: 'fas fa-dice',
          onclick: (ev) => this._onConfigureEntity(ev),
        },
      ].concat(buttons);
    }
    return buttons;
  }

  protected _onConfigureEntity(event: Event) {
    event.preventDefault();
    new game.swade.SwadeEntityTweaks(this.actor).render(true);
  }

  protected _calcInventoryWeight(items): number {
    let retVal = 0;
    items.forEach((i: any) => {
      retVal += i.data.weight * i.data.quantity;
    });
    return retVal;
  }

  private _toggleEquipped(id: string, item: any): any {
    return {
      _id: id,
      data: {
        equipped: !item.data.data.equipped,
      },
    };
  }

  protected async _chooseItemType(choices?: any) {
    if (!choices) {
      choices = {
        weapon: game.i18n.localize('ITEM.TypeWeapon'),
        armor: game.i18n.localize('ITEM.TypeArmor'),
        shield: game.i18n.localize('ITEM.TypeShield'),
        gear: game.i18n.localize('ITEM.TypeGear'),
        effect: 'Active Effect',
      };
    }
    const templateData = {
        types: choices,
        hasTypes: true,
        name: game.i18n
          .localize('ENTITY.New')
          .replace('{entity}', game.i18n.localize('ENTITY.Item')),
      },
      dlg = await renderTemplate(
        'templates/sidebar/entity-create.html',
        templateData,
      );
    //Create Dialog window
    return new Promise((resolve) => {
      new Dialog({
        title: game.i18n
          .localize('ENTITY.Create')
          .replace('{entity}', game.i18n.localize('ENTITY.Item')),
        content: dlg,
        buttons: {
          ok: {
            label: game.i18n.localize('ENTITY.CreateNew'),
            icon: '<i class="fas fa-check"></i>',
            callback: (html: JQuery) => {
              resolve({
                type: html.find('select[name="type"]').val(),
                name: html.find('input[name="name"]').val(),
              });
            },
          },
          cancel: {
            icon: '<i class="fas fa-times"></i>',
            label: game.i18n.localize('Cancel'),
          },
        },
        default: 'ok',
      }).render(true);
    });
  }

  protected async _createActiveEffect(name?: string) {
    let possibleName = game.i18n
      .localize('ENTITY.New')
      .replace('{entity}', game.i18n.localize('Active Effect'));
    if (name) possibleName = name;
    const id = (
      await this.actor.createEmbeddedEntity('ActiveEffect', {
        label: possibleName,
        icon: '/icons/svg/mystery-man-black.svg',
      })
    )._id;
    return this.actor['effects'].get(id).sheet.render(true);
  }
}
